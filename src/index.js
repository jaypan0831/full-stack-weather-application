const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

const express = require('express')
const path = require('path')
const hbs = require('hbs')

const app = express()
const port = process.env.PORT || 5001

// Define path for express config
const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')  

// Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(publicDirectoryPath)) 

app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather',
        name: 'Jay Pan'
    })
})

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About Me',
        name: 'Jay Pan'
    })
})

app.get('/weather', (req, res) => {
    const address = req.query.address

    if(!address)
        return res.send({error: 'you must provide a search term'})

    geocode(address, (error, { latitude, longtitude, location } = {}) => {
        if(error) return res.send({error})
        
        forecast(latitude, longtitude, (error, forecastData) => {
            if(error) return res.send({error})
            
            res.send({
                address,
                location,
                forecast: forecastData
            })
        })
    })
})

app.get('/about/*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'Jay Pan',
        errorMsg: 'About Article Not Found',
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'Jay Pan',
        errorMsg: 'Page Not Found',
    })
})


app.listen(port, () => console.log(`server is up on prot ${port}.`))