const request = require('request')

const geocode = (address, callback) => {
    const geocodeURL = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(address)}.json?access_token=pk.eyJ1IjoiamF5cGFuIiwiYSI6ImNqeHNzMjlnajBteHczbHJzc3g5ODN5OWMifQ.Js_oCCpHCV4mkOJ1ij6VvQ&limit=1`

    request({url: geocodeURL, json: true}, (err, { body }) => {
        if(err) {
            callback('Unable to connect to services.', undefined)
        } else if (body.features.length === 0) {
            callback('Unable to find location, please find another search', undefined)
        } else {
            callback(undefined, {
                latitude: body.features[0].center[1],
                longtitude: body.features[0].center[0],
                location: body.features[0].place_name
            })
        }
    })
}

module.exports = geocode