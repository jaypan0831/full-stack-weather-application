const request = require('request')

const forecast = (latitude, longtitude, callback) => {
    const url = `https://api.darksky.net/forecast/5e95d3fd42f2d60a83b660d4b8607cff/${latitude},${longtitude}?lang=zh-tw&units=si`

    request({ url, json: true }, (err, { body }) => {
        if(err) {
            callback('Unalbe to connect to weather service!', undefined)
        } else if (body.error) {
            callback('Unable to find location!', undefined)
        } else {
            callback(undefined, body.daily.data[0].summary)
        }
    })
}

module.exports = forecast